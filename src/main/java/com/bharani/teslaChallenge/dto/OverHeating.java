package com.bharani.teslaChallenge.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Value;

@Builder
@Value
public class OverHeating {
  @JsonProperty
  boolean overtemp;

  @JsonProperty("device_id")
  int deviceId;

  @JsonProperty("formatted_time")
  String timestamp;
}
