package com.bharani.teslaChallenge.controller;

import com.bharani.teslaChallenge.dto.OverHeating;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

@RestController
public class DataController {

  ArrayList<String> errors;

  DataController(){
    errors = new ArrayList<>();
  }

  @RequestMapping(value = "/errors", produces = "application/json")
  public ResponseEntity<Map<String, ArrayList<String>>> getErrors(){
    return ResponseEntity.status(HttpStatus.OK)
            .body(Collections.singletonMap("errors", errors));
  }

  @DeleteMapping("/delete")
  public ResponseEntity deleteErrors(){
    errors.clear();
    return ResponseEntity.status(HttpStatus.OK).build();
  }


  @PostMapping(value = "/temp", produces = "application/json")
  public ResponseEntity processTemperature(@RequestBody Map<String, String> input) {
    String dataString = input.getOrDefault("data", "");
    String[] values = dataString.split(":");

    if(values.length != 4 || !values[2].equals("'Temperature'")) {
      return processBadRequest(dataString);
    }

    try {
      int deviceId = Integer.parseInt(values[0]);
      long timestamp = Long.parseLong(values[1]);
      double temperature = Double.parseDouble(values[3]);
      DateFormat format = new SimpleDateFormat("y/M/d H:m:s");
      String formattedTimeStamp = format.format(new Date(timestamp));

      if(temperature >= 90){
        OverHeating response = OverHeating.builder()
                .deviceId(deviceId)
                .overtemp(true)
                .timestamp(formattedTimeStamp)
                .build();
        return ResponseEntity.status(HttpStatus.OK)
                .body(response);
      }
    }
    catch(NumberFormatException e){
      return processBadRequest(dataString);
    }

    return ResponseEntity.status(HttpStatus.OK)
            .body(Collections.singletonMap("overtemp", false));
  }


  public synchronized ResponseEntity<Map<String, String>> processBadRequest(String dataString){
    errors.add(dataString);
    return ResponseEntity.status(HttpStatus.BAD_REQUEST)
            .body(Collections.singletonMap("error", "bad request"));
  }
}
